Drupal.behaviors.PDFReviewColor = function(context) {
  var form = $('#pdfreview-admin-settings');
  form.find('.form-item').each(function(i){
    var current = $(this);
    var elem = $('<div class=""></div>').appendTo(current);
    var farb = $.farbtastic(elem, function(color){
      var textfield = current.find('input[type="text"]');
      var type = textfield.attr('id').split('-').pop();
      var hiddenfield = form.find('input:hidden[name*="' + type + '_text"]');
      var txtcolor = farb.RGBToHSL(farb.unpack(color))[2] > 0.5 ? '#000' : '#fff';
      textfield.css({'background-color': color, 'color': txtcolor});
      textfield.val(color);
      hiddenfield.val(txtcolor);
    });
  });
};