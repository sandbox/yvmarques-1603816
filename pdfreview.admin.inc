<?php
function pdfreview_admin_settings($form_state) {
  $base = drupal_get_path('module', 'pdfreview');
  $colors = variable_get('pdfreview_colors', array());

  // Add Farbtastic color picker
  drupal_add_css('misc/farbtastic/farbtastic.css', 'module', 'all', FALSE);
  drupal_add_js('misc/farbtastic/farbtastic.js');

  drupal_add_js($base .'/color.js');

  $form = array();

  $types = array(
    'page' => 'Page',
    'paragraph' => 'Paragraphs',
    'highlight' => 'Highlight',
    'zone' => 'Zone'
  );

  $form['pdfreview_colors']['#tree'] = TRUE;
  foreach($types as $type => $name) {
    $form['pdfreview_colors'][$type] = array(
      '#type' => 'textfield',
      '#title' => $name,
      '#default_value' => check_plain($colors[$type]),
      '#size' => 8
    );
    $form['pdfreview_colors'][$type . '_text'] = array(
      '#type' => 'hidden',
      '#default_value' => check_plain($colors[$type . '_text']),
    );
  }
  return system_settings_form($form);
}