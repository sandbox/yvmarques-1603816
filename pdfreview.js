// @todo Switch the code to Backbone.js

;(function($){
  Drupal.behaviors.PDFReview = function(context) {
    var pages = $('#pdfviewer');
    var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
    var tool = $('#pdfviewer .review-tool');
    var grid = $('#pdfviewer .review-grid');
    var img = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' img');
    var reviews = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' .review-item');
    var timers = {};
    var btnCancel = $('#pdfviewer .review-tool .menu .cancel');
    var btnMarks = $('#pdfviewer .review-tool .menu .marks');
    var btnCsvAndPrint = $('#pdfviewer .review-tool .menu .csv, #pdfviewer .review-tool .menu .print');
    var btnHideReviewAndDelete = $('#pdfviewer .review-item .links .link.hide, #pdfviewer .review-item .links .link.delete');
    var btnManage = $('#pdfviewer .review-tool .menu .manage');
    
    // !Manage Button
    btnManage.click(function(e){
      var bt = $(this);
      var url = bt.attr('href');
      Drupal.PDFReview.dialogManage(url);
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
    
    // !CSV Export Button And Print
    btnCsvAndPrint.click(function(e){
      var url = $(this).attr('href');
      window.location = url;
      e.preventDefault();
      e.stopPropagation();
      return false;
    });

    // !Hide this review & Delete
    btnHideReviewAndDelete.live('click', function(e){
      var bt = $(this);
      var review = bt.parent().parent();
      var reviews = review.parent();
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = current.find('img');
      var tool = $('#pdfviewer .review-tool');
      var count_ref = reviews.find('.review-item.ref-count').attr('data-rid');
      var span_count = current.find('.count[data-ref="' + count_ref + '"]');
      var num_count = parseInt(span_count.find('span').html());

      $.get(bt.attr('href'), {'doc': Drupal.settings.PDFViewer.doc}, function(data){
        review.removeClass('open').addClass('review-hidden');

        if(num_count - 1 > 0) {
          span_count.find('span').html(num_count - 1);
        }else{
          span_count.remove();
        }

        if(reviews.find('.review-item:not(.review-hidden)').size() == 0) {
          current.find('.word').removeClass('selected').hide().die();
          var api = img.data('Jcrop'); // A trick to get acces to the API
          if(api) {
            api.destroy();
          }
          reviews.addClass('review-block-hidden');          
        }
        Drupal.PDFReview.message(data.message);
      }, 'json');

      e.preventDefault();
      e.stopPropagation();
      return false;
    });

    // !Button Cancel
    btnCancel.click(function(e){
      var bt = $(this);
      if(!bt.hasClass('clicked')) {
        var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
        var img = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' img');
        var form = $('#review-form');
  
        current.find('.word').removeClass('selected').hide().die();
  
        var api = img.data('Jcrop'); // A trick to get acces to the API
        if(api) {
          api.destroy();
        }
  
        form.fadeOut('fast');
  
        bt.removeClass('bt').addClass('clicked');
      }
      e.preventDefault();
      e.stopPropagation();
      return false;
    });

    // !Button Marks
    btnMarks.click(function(e){
      var bt = $(this);
      var marks = $('#pdfviewer .item span.count');
      var hideMessage = Drupal.t("Hide markers");
      var showMessage = Drupal.t("Show markers");
      if(bt.hasClass('clicked')) {
        marks.show();
        bt.find('span').empty().html(hideMessage);
        bt.removeClass('clicked').addClass('bt');
      } else {
        marks.fadeOut();
        bt.find('span').empty().html(showMessage);
        bt.removeClass('bt').addClass('clicked');
      }
      e.preventDefault();
      e.stopPropagation();
      return false;
    });

    // !Button Filter
    $('#pdfviewer .review-tool .menu .filter').click(function(){
      var bt = $(this);
      var options_filter = $('#pdfviewer .review-tool .options.filter');
      timers.filter = null;

      options_filter.hide();
      if(bt.hasClass('bt')) {
        bt.removeClass('bt').addClass('clicked');
        options_filter.show();
      }else{
        bt.removeClass('clicked').addClass('bt');
        options_filter.hide();
      }
      return false;
    });

    // !Button Pages
    $('#pdfviewer .review-tool .menu .pages').click(function(){
      var bt = $(this);
      var options_pages = $('#pdfviewer .review-tool .options.pages');
      var timer = null;

      options_pages.hide();
      if(bt.hasClass('bt')) {
        bt.removeClass('bt').addClass('clicked');
        options_pages.show();

        // Set a timer to avoid opening options infite
        timer = setTimeout(function(){options_pages.fadeOut(); bt.removeClass('clicked').addClass('bt');}, 1000);
        options_pages.mouseenter(function(){ 
          if(timer) {
            clearTimeout(timer);
          }
        }).mouseleave(function(){
          timer = setTimeout(function(){options_pages.fadeOut(); bt.removeClass('clicked').addClass('bt');}, 1000);
        });

      }else{
        bt.removeClass('clicked').addClass('bt');
        options_pages.hide();
      }
      return false;
    });

    // !Button Reviews
    $('#pdfviewer .review-tool .menu .reviews').click(function(){
      var bt = $(this);
      var current = $('#pdfviewer .item');
      var img = $('#pdfviewer .item img');
      var reviews = $('#pdfviewer .item .reviews-block');
      var counts = current.find('.count');
      var margin  = (parseInt(current.width()) - parseInt(img.width())) - 6;


      if(bt.hasClass('bt')) {
        bt.removeClass('bt').addClass('clicked');
        img.animate({marginLeft: margin}, 'fast');
        counts.animate({'left': '+=' + (margin/2)}, 'fast');
        reviews.css({
          'top': img.css('margin-top'),
          'height': parseInt(img.height()) - (parseInt(reviews.css('padding-top')) * 2)
        }).fadeIn('slow');
        current.addClass('reviews-open');
        if(!current.hasClass('text-processed')) {
          Drupal.PDFViewer.loadText(current, Drupal.PDFReview.page, img);
        }
      }else{
        Drupal.PDFReview.close();
      }
      
      return false;
    });

    // !Add option pages
    var options_pages = $('#pdfviewer .review-tool .options.pages');
    if(!options_pages.hasClass('processed')){
      var nb_pages = parseInt($('#pdfviewer').attr('data-pages'));
      var options_pages_menu = options_pages.find('.menu');
      for(p = 1; p <= nb_pages; p++) {
        var menu_item  = $('<a>').attr('href', '#p' + p).text('Page ' + p);
        if(p == 1) {
          menu_item.addClass('first');
        }
        if(p == nb_pages) {
          menu_item.addClass('last');
        }

        var location_hash = window.location.hash;
        if((location_hash == '#p' + p) || (location_hash.length == 0 && p == 1)) {
          menu_item.addClass('selected');
        }

        menu_item.click(function(){
          $('#pdfviewer .review-tool .menu .pages').removeClass('clicked').addClass('bt');
          $(this).parent().find('a').removeClass('selected');
          $(this).addClass('selected');
          options_pages.hide();
        });
        options_pages_menu.append(menu_item);
      }
      options_pages.addClass('processed');
    }
    
    // !Manage filter
    $('#pdfreview-filters').submit(function(e){
      Drupal.PDFReview.resetFilters(function(){
        $('#pdfreview-filters [type="checkbox"]:not(:checked)').each(function(){
          var checkbox = $(this);
          var type = checkbox.attr('name').split('[')[0];
          var value = checkbox.val();
          Drupal.PDFReview.applyFilter(type, value);
        });
      });
      $('#pdfviewer .review-tool .options.filter').hide();
      $('#pdfviewer .review-tool .menu .filter').removeClass('clicked').addClass('bt');
      e.stopPropagation();
      e.preventDefault();
    });
    
    $('#pdfreview-filters [id*="cancel"]').click(function(e){
      $('#pdfviewer .review-tool .menu .filter').trigger('click');
      e.preventDefault();
      e.stopPropagation();
    });
    
    
    // Options (add reviews)
    /* !Reviews actions */
    $('#pdfviewer .item').live('dblclick', function(e){
      Drupal.PDFReview.close(); // Close panel reviews
      
      // Addition for options
      var add = 0;
      $('#pdfviewer .item').each(function(i){
        if(parseInt($(this).attr('data-page')) >= (Drupal.PDFReview.page)) {
          return false;
        }
        add += $(this).height();
      });
      
      // Some variables
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = current.find('img');
      var form = $('#review-form');
      var options = $('#review-form-options');
      var top = (e.pageY + add) - current.offset().top;
      var left = e.pageX - current.offset().left;
      var timer;

      // We load the text because we need it here.
      if(!current.hasClass('text-processed')) {
        Drupal.PDFViewer.loadText(current, Drupal.PDFReview.page, img);
      }
      
      // Check that we've text.. otherwise its
      // totally useless to show the menu.
      $('#review-form-options a').removeClass('disabled');
      if(current.find('.word').size() == 0) {
        // message = Drupal.t("Unable to find text on this page. Try again.");
        // Drupal.PDFReview.message(message, true);
        // If we don't have text we allow at least comment that doesn't need
        // text to work
        $('#review-form-options a').each(function(i){
          var link = $(this);
          var action = link.attr('data-action');
          if(action == 'highlight' || action == 'paragraph') {
            link.addClass('disabled');
          }
        });
      }
      
      // Display options
      options.css({'top': (top - 25), 'left': (left + 14)}).show();
      
      // Set a timer to avoid opening options infite
      timer = setTimeout(function(){options.fadeOut();}, 1000);
      options.mouseenter(function(){ 
        if(timer) {
          clearTimeout(timer);
        }
      }).mouseleave(function(){
        timer = setTimeout(function(){options.fadeOut();}, 1000);
      });
      
      e.stopPropagation();
      e.preventDefault();
      return false;
    });

    // Action 'Comment' options action
    /* !Add review options */
    $('#review-form-options a').each(function(i){
      var link = $(this);

      link.click(function(e){
        Drupal.PDFReview.close(); // Close panel reviews

        // Some variables
        var action = link.attr('data-action');
        var form = $('#review-form');
        var options = $('#review-form-options');
        var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
        var img = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' img');
        
        if(link.hasClass('disabled')) {
          message = Drupal.t("Unable to find text on this page. Try again.");
          Drupal.PDFReview.message(message, true);
          options.fadeOut('fast');
          return;
        }
        

        btnCancel.removeClass('clicked').addClass('bt');
        // Addition height
        var add = 0;
        $('#pdfviewer .item').each(function(i){
          if(parseInt($(this).attr('data-page')) >= (Drupal.PDFReview.page)) {
            return false;
          }
          add += $(this).height();
        });

        // Differents actions
        switch(action) {
          // Open comment for a page
          case'page':
            $('#pdfviewer').scrollTop(add);
            options.fadeOut('fast');
            var top = parseInt(img.css('margin-top'));
            var left = (parseInt(current.width()) - parseInt(img.width())) / 2;
            var data = {
              'doc': Drupal.settings.PDFViewer.doc,
              'page': Drupal.PDFReview.page,
              'nid': Drupal.settings.PDFViewer.nid,
              'type': action,
              'region': 1,
              'coords': {
                'top': 0,
                'left': left,
                'page_id': Drupal.PDFReview.page
              }
            };

            form.css({'top': (top + add), 'left': left});
            form.find('input[name="data"]').val(JSON.stringify(data));
            form.show();
          break;
          // Comment a paragraph
          case'paragraph':
            options.fadeOut('fast');
            var all = current.find('.word');
            all.show();

            var timer;

            all.live('mouseover', function(e) {
              var words = $('.' + $(this).attr('data-paragraph'), current);
              words.css('cursor', 'pointer');
              if(form.css('display') == 'none') {
                words.addClass('selected');
              }
            });

            all.live('mouseout', function(e) {
              var words = $('.' + $(this).attr('data-paragraph'), current);
              words.css('cursor', 'pointer');
              if(form.css('display') == 'none') {
                words.removeClass('selected');
              }
            });

            all.live('click', function(e) {
              var words = $('.' + $(this).attr('data-paragraph'), current);
              var top = parseInt(words.filter(':last').css('top')) + parseInt(words.filter(':last').css('height'));
              var left = parseInt(words.filter(':last').css('left'));
              var data = {
                'doc': Drupal.settings.PDFViewer.doc,
                'page': Drupal.PDFReview.page,
                'nid': Drupal.settings.PDFViewer.nid,
                'type': action,
                'region': Drupal.PDFReview.getRegion(img, parseInt(words.eq(0).css('top'))),
                'coords': {
                  'top': parseInt(words.eq(0).css('top')),
                  'left': parseInt(words.eq(0).css('left')),
                  'paragraph_id': $(this).attr('data-paragraph')
                }
              };
              form.css({'top': (top + add), 'left': left});
              if (Drupal.PDFReview.isFormHidden()) {
                Drupal.PDFReview.setNewLeft(left);
              }
              form.find('input[name="data"]').val(JSON.stringify(data));
              form.show();
            });
          break;
          // !Comment highlight
          case'highlight':
            options.fadeOut('fast');
            img.select(function(){ return false; });
            var all = current.find('.word');
            all.show();
            var dragInit = false;
            var data = {
              'doc': Drupal.settings.PDFViewer.doc,
              'page': Drupal.PDFReview.page,
              'nid': Drupal.settings.PDFViewer.nid,
              'type': action,
              'coords': {}
            };

            all.css('cursor', 'text');

            all.live('mousedown', function(e) {
              dragInit = true;
              all.removeClass('selected');
              form.fadeOut('fast');
            });

/**/
            all.live('mousemove', function(e) {
              if(dragInit) {
                var selection = Drupal.PDFReview.selection();
                var anchor = selection.anchorNode;
                var focus = selection.focusNode;
                // window.setTimeout(function(){
                  Drupal.PDFReview.applySelect(current, anchor, focus);
                // }, 100);
              }
            });
/**/

            all.live('mouseup', function(e) {
              var start, end;
              var selection = Drupal.PDFReview.selection();
              var anchor = selection.anchorNode;
              var focus = selection.focusNode;
              
              Drupal.PDFReview.applySelect(current, anchor, focus, function(){
                var words = [];
                var top = false;
                var left = false;
  
                // We get all ids for the display after.
                current.find('.word.selected').each(function(){
                  if(!top) {
                    top = $(this).css('top');
                    left = $(this).css('left');
                  }
                  words.push($(this).attr('id'));
                });
  
                data.coords = {
                  'top': parseInt(top),
                  'left': parseInt(left),
                  'ids': words.join(',')
                };
                data.region = Drupal.PDFReview.getRegion(img, parseInt(top));

                // all.die();
  
                // And we show the form at the last word
                var top = parseInt(current.find('.word.selected:last').css('top')) + parseInt(current.find('.word.selected:last').css('height'));
                var left = parseInt(current.find('.word.selected:last').css('left'));
                form.css({'top': (top + add), 'left': left});
                if (Drupal.PDFReview.isFormHidden()) {
                  Drupal.PDFReview.setNewLeft(left);
                }
                form.find('input[name="data"]').val(JSON.stringify(data));
                form.show();
                selection.removeAllRanges();
                dragInit = false;
              });
            });
          break;
          // !Add Comment zone
          case'zone':
            options.fadeOut('fast');
            img.Jcrop({
              keySupport: false,
              onSelect: function(coords){
                var data = {
                  'doc': Drupal.settings.PDFViewer.doc,
                  'page': Drupal.PDFReview.page,
                  'nid': Drupal.settings.PDFViewer.nid,
                  'type': action,
                  'region': Drupal.PDFReview.getRegion(img, (coords.y + parseInt(img.css('margin-top')))),
                  'coords': {
                    'top': (coords.y + parseInt(img.css('margin-top'))),
                    'left': (coords.x + parseInt(img.css('margin-left'))),
                    'y': coords.y,
                    'x': coords.x,
                    'y2': coords.y2,
                    'x2': coords.x2,
                    'h': coords.h,
                    'w': coords.w
                  }
                };

                var left = (coords.x + 85)
                form.css({'top': (coords.y + coords.h + add + 15), 'left': left});
                if (Drupal.PDFReview.isFormHidden()) {
                  Drupal.PDFReview.setNewLeft(left);
                }
                form.find('input[name="data"]').val(JSON.stringify(data));
                form.show();
              },
              onRelease: function(){
                form.hide();
              }
            }, function(){
              $('.jcrop-holder').css({
                'left': '50%',
                'marginLeft': -(img.width() / 2),
                'marginTop': img.css('margin-top'),
                'marginBottom': img.css('margin-bottom')
              });
            });
          break;
        }
        e.stopPropagation();
        e.preventDefault();
      });
    });
    
    // Load more comments
    $('#pdfviewer .reviews-block a.collapsed').live('click', function(event) {
      switch(event.type) {
        case 'click':
          var item = $(this);
          var reviews = $(this).parent();
          var siblings = reviews.siblings('.review-block');

          reviews.find('.hidden:not(.filtred)').removeClass('hidden');
          item.hide();
          var current_top = parseInt(reviews.css('top'));
          var current_height = parseInt(reviews.height());
          height_total = current_height + current_top;
          new_top = false;
          $.each(siblings, function(index, value){
            elem = $(value);
            elem_top = parseInt(elem.css('top'));
            if (elem_top >= current_top) {
              if(!new_top) {
                new_top = (height_total - elem_top) + 10;
              }
              if (new_top < 0) { return; }
              elem.animate({'top': '+=' + new_top});
            }
          });
          event.stopPropagation();
        break;
      }
    });

    // !Hover reviews
    /*
    $('#pdfviewer .item .review-block .review-item').live('mouseover', function(event){
      var review = $(this);
      var links = review.find('.links');
      links.show();
    });

    $('#pdfviewer .item .review-block .review-item').live('mouseout', function(event){
      var review = $(this);
      if(!review.hasClass('open')) {
        var links = review.find('.links');
        links.hide();
      }
    });
    */

    // !Click reviews
    $('#pdfviewer .item .review-block .review-item .info span.a').live('click', function(event){
      // Some variables
      var review = $(this).parents('.review-item');
      var reviews = review.parent();
      var siblings = reviews.siblings('.review-block');
      var column = reviews.parent();
      var hasOpen = column.find('.open').size();
      var opened = column.find('.open');
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' img');
      var type = reviews.attr('data-type');
      var counts = current.find('.count');
      var fullview = $('#review-fullview');
      var rid = parseInt(review.attr('data-rid'));

      if(!review.hasClass('open') && hasOpen > 0) {
        opened.find('.info').trigger('click');
      }

      review.find('.text').slideToggle(function(){
        var current_top = parseInt(reviews.css('top'));
        var current_height = parseInt(reviews.height());
        sibs_top = false;
        if (review.hasClass('open')) {
          height_total = current_height + current_top;
          sibs_top = [];
        }

        new_top = false;
        $.each(siblings, function(index, value){
          elem = $(value);
          elem_top = parseInt(elem.css('top'));
          if (elem_top >= current_top) {
            if(review.hasClass('open')) {
              sibs_top[index] = elem_top;
              if(!new_top) {
                new_top = (height_total - elem_top) + 10;
              }
              if (new_top < 0) { return; }
              elem.animate({'top': '+=' + new_top});
            } else {
              if (sibs_top && sibs_top[index].length > 0) {
                elem_top = sibs_top[index];
                elem.animate({'top': elem_top});
              }
            }
          }
        });
      });

      if(review.hasClass('open')) {
        review.removeClass('open').addClass('close');
        review.find('span.ui-icon').removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
        reviews.css('z-index', 310);
        if(fullview.css('display') == 'block') {
          fullview.find('input[id*="edit-cancel"], .close').trigger('click');
        }
      }else{
        review.removeClass('close').addClass('open');
        review.find('span.ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
        // review.find('.links').show();
        reviews.css('z-index', 320);
        Drupal.PDFReview.read(rid);
        if (review.find('span.marker').size() > 0) {
          review.find('span.marker').remove();
        }
      }
    
      // !Open a zone
      if(type == 'zone') {
        var coords = reviews.attr('data-area').split(',');
        if(img.css('display') == 'none') {
          var api = img.data('Jcrop'); // A trick to get acces to the API
          api.destroy();
          reviews.parent().css('top', function() {
            return parseInt(this.style.top) + parseInt(img.css('margin-top'));
          });
          counts.css('top', function() {
            return parseInt(this.style.top) + parseInt(img.css('margin-top'));
          });
        }else{
          img.Jcrop({
            keySupport: false,
            allowSelect: false,
            allowMove: false,
            allowResize: false,
            setSelect: coords,
          }, function(api){
            reviews.parent().css('top', function() {
              return parseInt(this.style.top) - parseInt(img.css('margin-top'));
            });
            counts.css('top', function() {
              return parseInt(this.style.top) - parseInt(img.css('margin-top'));
            });
            $('.jcrop-holder').css({
              'marginLeft': (parseInt(img.css('margin-left')) + 3),
              'marginTop': img.css('margin-top'),
              'marginBottom': img.css('margin-bottom')
            });
          });
        }
      }
    
      // !A paragraph
      if(type == 'paragraph') {
        var words = $('.' + reviews.attr('data-paragraph') + '.word', current);
        var addition = parseInt(img.css('margin-left')) / 2;
    
        if(words.eq(0).css('display') == 'none') {
          words.show().addClass('selected');
          words.each(function(){
            var left = parseInt($(this).css('left'));
            $(this).css('left', (left + addition));
          });
        }else{
          words.each(function(){
            var left = parseInt($(this).css('left'));
            $(this).css('left', (left - addition));
          });
          words.removeClass('selected').hide();
        }
      }
    
      // !Open a highlights
      if(type == 'highlight') {
        var ids = '#' + reviews.attr('data-ids').split(',').join(',#');
        var addition = parseInt(img.css('margin-left')) / 2;
        if(!review.hasClass('close')) {
          $(ids).each(function(i) {
            var word = $(this);
            word.show();
            var left = parseInt($(this).css('left'));
            word.css('left', (left + addition)).addClass('selected');
          });
        }else{
          $(ids).each(function(i) {
            var word = $(this);
            var left = parseInt(word.css('left'));
            word.css('left', (left - addition)).removeClass('selected');
            word.hide();
          });
        }
      }
      event.stopPropagation();
      event.preventDefault();
    });

    /* !Fullview */
    $('#pdfviewer .reviews-block a[name="reply"]').live('click', function(e){
      var link = $(this);
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = current.find('img');
      var comment = $(this).parent().parent();
      var review = comment.parent();
      var reviews = review.parent();
      var fullview = $('#review-fullview');
      var rid = parseInt(comment.attr('data-rid'));
      var loader = fullview.find('.comments img').clone();
      
      if(fullview.css('display') == 'block') {
        fullview.find('.close').trigger('click');
        e.stopPropagation();
        e.preventDefault();
        return false;
      }

      // get rid
      fullview.attr('data-rid', rid);

      // Addition height
      var add = 0;
      $('#pdfviewer .item').each(function(i){
        if(parseInt($(this).attr('data-page')) >= (Drupal.PDFReview.page)) {
          return false;
        }
        add += $(this).height();
      });

      var top = (e.pageY + add) - (current.offset().top + 40);
      var left = parseInt(img.css('margin-left')) + 10;
      fullview.css({'top': top, 'left': left}).show();
      fullview.find('form input[name="rid"]').val(rid);

      Drupal.PDFReview.loadReplies(fullview, link.attr('href'));

      e.stopPropagation();
      e.preventDefault();
      return false;
    });

    $('#review-fullview').find('input[id*="edit-cancel"], .close').click(function(e){
      var fullview = $('#review-fullview');
      var loader = fullview.find('.comments img').clone();

      fullview.fadeOut('fast');
      fullview.find('.comments').empty().append(loader);
      return false;
    });
    
    $('#review-fullview').find('.grid').mouseenter(function(e){
      var fullview = $('#review-fullview');
      fullview.animate({'opacity': 0.1});
    }).mouseleave(function(e){
      var fullview = $('#review-fullview');
      fullview.animate({'opacity': 1});
    });

    $('#review-fullview').find('form').submit(function(e){
      var fullview = $('#review-fullview');
      var rid = parseInt(fullview.attr('data-rid'));
      var review = $('#review-' + rid);
      var reply_span = review.find('a[name="reply"] span');
      
      $('#review-form-iframe').unbind('load').load(function (e){
        var response = $(this).contents().find('body').text();
        var data = Drupal.parseJson(response);

        if (data['error']) {
          Drupal.PDFReview.message(data.message, true);
          return;
        }
        
        var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
        var img = current.find('img');
        var tool = $('#pdfviewer .review-tool');
        var form = $('#review-form form');

        Drupal.PDFReview.message(data.message);
        form.find('textarea').val('');
        form.find('input[type="text"], input[type="file"]').val('');

        fullview.fadeOut('fast');
        var nb = parseInt(reply_span.html());
        reply_span.html((nb + 1));

        //Drupal.PDFReview.loadReplies(fullview, rid);
      });
    });
    
    // If the user click somewhere else, hide options
    // !Hide Options
    $('#pdfviewer .item img').live('click', function(e){
      var form = $('#review-form');

      $('#review-form-options').fadeOut('fast');
      if(form.find('textarea').val() == '') {
        $('#review-form').fadeOut('fast');
      }
      return false;
    });
    
    // !Fix the review-tool bar
    Drupal.PDFReview.fixed(tool, pages, false);
    $(window).scroll(function(){
      Drupal.PDFReview.fixed(tool, pages, true);
    });
    
    // !Grid
    Drupal.PDFReview.fixedGrid(grid, pages, false, tool);
    $(window).scroll(function(){
      Drupal.PDFReview.fixedGrid(grid, pages, true, tool);
    });

    // !Cancel button
    $('#review-form form input[id*="cancel"]').live('click', function(){
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = $('#pdfviewer .page-' + Drupal.PDFReview.page + ' img');
      var form = $('#review-form');

      current.find('.word').removeClass('selected').hide().die();

      var api = img.data('Jcrop'); // A trick to get acces to the API
      if(api) {
        api.destroy();
      }

      form.fadeOut('fast');
      btnCancel.removeClass('bt').addClass('clicked');
      
      form.find('form').each(function(){
        this.reset();
      });
      $('#edit-source-link-wrapper, #edit-source-file-wrapper').hide();
      $('#review-form div.triangle').css('margin-left', '25px');
      
      return false;
    });

    // !Submit
    $('#review-form form').submit(function(e){
      var form = $(this);
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      var img = current.find('img');
      var tool = $('#pdfviewer .review-tool');
    
      $('#review-form-iframe').unbind('load').load(function (e){
        var response = $(this).contents().find('body').text();
        var data = Drupal.parseJson(response);

        if (data['error']) {
          Drupal.PDFReview.message(data.message, true);
          return;
        }
        
        var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
        var img = current.find('img');
        var tool = $('#pdfviewer .review-tool');
        var form = $('#review-form form');

        Drupal.PDFReview.message(data.message);
        form.find('textarea').val('');
        form.find('input[type="text"], input[type="file"]').val('');

        current.find('.word').removeClass('selected').hide().die();
        var api = img.data('Jcrop'); // A trick to get acces to the API
        if(api) {
          api.destroy();
        }
        $('#review-form').fadeOut('fast');
        current.find('.reviews-block').remove();
        current.find('.count').remove();
        current.removeClass('review-processed'); // In case of we can't retreive
        Drupal.PDFReview.reviews(Drupal.PDFReview.page, tool, current);
      });
    });

    // !Count action
    $('#pdfviewer .item span.count').click(function(e){
      var count_elem = $(this);
      var rid = parseInt(count_elem.attr('data-ref'));
      var bt = $('#pdfviewer .review-tool .menu .btleft');
      var review = $('#review-' + rid);
      var reviews = review.parent();
      var previous_rid = false;
      if (previous_rid != rid || !bt.hasClass('clicked')) {
        if (!bt.hasClass('clicked')) {
          bt.trigger('click');
        }
        window.setTimeout(function(){
          if(reviews.find('.collapsed').size() > 0) {
            reviews.find('.collapsed').trigger('click');
          }
          review.find('.info').trigger('click');
        }, 500);
        previous_rid = rid;
      } else {
        review.find('.info').trigger('click');
        bt.trigger('click');
      }
    });
    
    // !Source Information change
    $('#edit-source-description').keydown(function(){
      $('#edit-source-link-wrapper, #edit-source-file-wrapper').fadeIn();
    });
  };

  Drupal.PDFReview = {
    cache: [],
    page: 0,
    timer: false,
    load: function(page) {
      // Elements
      var pages = $('#pdfviewer');
      var elem = pages.find('.page-'+ page);
      var previous = pages.find('.page-'+ (page - 1));
      var tool = $('#pdfviewer .review-tool');
      
      // Set current page
      this.page = page;
      if(this.timer) {
        window.clearTimeout(this.timer);
      }
      
      tool.find('strong span').empty().html(page);
      if(this.cache[page]) {
        var bt = tool.find('.btleft');
        bt.find('span').empty().html('(' + this.cache[page] + ')');
        if(!elem.hasClass('reviews-open')) {
         bt.removeClass('clicked').addClass('bt');
        }else{
          bt.removeClass('bt').addClass('clicked');
        }
      }
      
      // We can know load all review for current page
      /*
      if(!elem.hasClass('review-processed')) {
        this.timer = window.setTimeout(function(){
          Drupal.PDFReview.reviews(page, tool, elem);
        }, 1000);
      }
      */
      
      // We change the selected page number
      var options_pages = $('#pdfviewer .review-tool .options.pages');
      options_pages.find('a').removeClass('selected');
      var current_page = options_pages.find('a[href="#p' + page + '"]');
      current_page.addClass('selected');

    },
    fixed: function(tool, pages, isScroll) {
      var offset = pages.parent().offset();
      if(isScroll) {
        offset.top -= $(window).scrollTop();
      }
      tool.css({position:'fixed', 'top': offset.top, 'left': offset.left, 'width': pages.width()});
    },
    fixedGrid: function(grid, pages, isScroll, tool) {
      if (!grid.hasClass('processed')) {
        var item = pages.find('.item.first img');
        var number = 8;
        var region_size = (parseInt(item.height()) / number);
        var height = item.height() + parseInt(item.css('margin-top'));
        var margin = (parseInt(pages.parent().width()) - parseInt(item.width())) / 2;
        grid.css('height', height + 'px');
        grid.css('left', margin + 'px');
        for(i = 1; i <= number; i++) {
          var css_class = 'grid-elem';
          if (i == 1) {
            css_class += ' first';
          }
          if (i == number) {
            css_class += ' last';
          }
          var elem = $('<div class="' + css_class + '" style="height:' + region_size + 'px"><p>' + i + '</p></div>').appendTo(grid);
        }
        grid.addClass('processed');
      }
      /*
      var offset = pages.parent().offset();
      if(isScroll) {
        offset.top -= $(window).scrollTop();
      }
      grid.css({position:'fixed', 'top': (offset.top + tool.height() + 5), 'left': offset.left, 'width': pages.width()});
      */
    },
    reviews: function(page, tool, elem, callback) {
      $.getJSON(Drupal.settings.basePath + 'reviews/get', {'doc': Drupal.settings.PDFViewer.doc, 'page': page}, function(data, textStatus){
        tool.find('.btleft span').empty().html('(' + data.nb +')');

        // Add count span to all top        
        $.each(data.counts, function(i){
          var count = data.counts[i];
          // var img = elem.find('img');
          // var left = (parseInt(elem.width()) - parseInt(img.width())) / 2;

          if (parseInt(count.count) > 0) {
            var span = $('<span>').html(count.count);
            $('<span>').css({
              'position': 'absolute',
              'top': (count.data.top),
              'left': count.data.left
            }).attr({
              'class': 'count'
            }).append(span).appendTo($('#pdfviewer .page-'+ page)).click(function(e){
              var bt = $('#pdfviewer .review-tool .menu .btleft');
              var review = $('#review-' + count.data.id);
              var reviews = review.parent();
              if (!bt.hasClass('clicked')) {
                bt.trigger('click');
                window.setTimeout(function(){
                  if(reviews.find('.collapsed').size() > 0) {
                    reviews.find('.collapsed').trigger('click');
                  }
                  review.find('.info').end().trigger('click');
                }, 500);
              } else {
                bt.trigger('click');
              }
            });
          } // endif parseInt(count)
        });
        
        Drupal.PDFReview.cache[page] = data.nb;
        $('#pdfviewer .page-'+ page).append(data.output);
      });
      $('#pdfreview-filters').trigger('submit');
      elem.addClass('review-processed');
    },
    allReviews: function(){
      $.getJSON(Drupal.settings.basePath + 'reviews/get', function(pages, textStatus){
        $.each(pages, function(i){
          var data = pages[i];
          var page = $('#pdfviewer .page-' + data.page);
          page.append(data.reviews);
          page.append(data.counts);
          Drupal.PDFReview.cache[data.page] = data.nb;
        });
      });
    },
    close: function() {
      var current = $('#pdfviewer .item');
      if(current.hasClass('reviews-open')) {
        var img = $('#pdfviewer .item img');
        var column = $('#pdfviewer .item .reviews-block');
        var bt = $('#pdfviewer .review-tool .menu .btleft');
        var counts = current.find('.count');
        var margin  = (parseInt(current.width()) - parseInt(img.width())) - 6;
        var fullview = $('#review-fullview');
        
        if(fullview.css('display') == 'block') {
          fullview.fadeOut('fast');
        }

        column.find('.open .info').trigger('click');
        column.fadeOut();
        img.animate({marginLeft: '0px'}, 'slow');
        counts.animate({'left': '-=' + (margin/2)}, 'slow');
        current.removeClass('reviews-open');
        bt.removeClass('clicked').addClass('bt');
      }
    },
    selection: function(){
      var select = null;
      if(window.getSelection) {
        select = window.getSelection();
      }else if(document.getSelection) {
        select = document.getSelection();
      }else{
        select = document.selection;
      }
      return select;
    },
    applySelect: function(current, anchor, focus, callback) {
      if( !(anchor || focus) ) {
        Drupal.PDFReview.message(Drupal.t("Impossible to get the selection. Please try with another Internet Browser."), true);
        return;
      }
  
      // Start word
      do{
        if (anchor.dataset && anchor.dataset.word) {
          start = anchor.dataset.word;
          break;
        }
        anchor = anchor.parentElement || anchor.parentNode;
      }while(anchor.parentElement || anchor.parentNode);
  
      // End word
      do{
        if (focus.dataset && focus.dataset.word) {
          end = focus.dataset.word;
          break;
        }
        focus = focus.parentElement || focus.parentNode;
      }while(focus.parentElement || focus.parentNode);
  
      start = start.split('-');
      if(end) {
        end = end.split('-');
      }
      var b = {};
      var e = {};
      b.p = parseInt(start[0]);
      b.w = parseInt(start[1]);
      e.p = parseInt(end[0]);
      e.w = parseInt(end[1]);
  
      // Swap data if needed (in case of inversed selection)
      if(e.p < b.p) {
        var swap = [];
        swap[0] = e.p;
        swap[1] = e.w;
  
        e.p = b.p;
        e.w = b.w;
  
        b.p = swap[0];
        b.w = swap[1];
      }
      
      if(e.p == b.p && e.w < b.w) {
        var swap;
        swap = e.w;
        e.w = b.w;  
        b.w = swap;
      }
  
      for (i = b.p; i <= e.p; i++) {
        var max = $('.paragraph-' + i + '.word', current).size();
        for (j = (i == b.p ? b.w : 0); j <= (i == e.p ? e.w : max); j++) {
          var elem = $('.paragraph-' + i + '.word-' + j, current);
          if(elem.hasClass('selected')) {
            // elem.removeClass('selected');
            continue;
          }else{
            elem.addClass('selected');
          }
        }
      }

      // Callback
      if( typeof callback == 'function') {
        callback.apply();
      }
    },
    saveReview: function(form, callback) {
      form.find('form input[name="is_ajax"]').val('1');
      var params = form.serialize();
      var $this = this;
      if(params.length > 0) {
        $.post(Drupal.settings.basePath + 'reviews/post', params, function(data){
          $this.message(data.message);
          form.find('textarea').val('');
          form.find('input[type="text"]').val('');
          if(typeof callback == 'function') {
            callback.call();
          }
        }, 'json');
      } else {
        this.message(Drupal.t("An error occur. Please try again."), true);
      }
    },
    loadReplies: function(fullview, url) {
      var params = {
        'doc': Drupal.settings.PDFViewer.doc,
        'page': Drupal.PDFReview.page
      };
      $.getJSON(url, params, function(data){
        fullview.find('.comments').empty().html(data.output);
        Drupal.PDFReview.initQuickLook();
      });
      return false;
    },
    message: function(msg, error) {
      var viewer = $('#pdfviewer');
      offset = viewer.offset();
      var message = viewer.find('.messages');
      message.css({
        'position': 'fixed',
        'top': parseInt(offset.top) + 30
      });
      if(error) {
        message.addClass('error');
      }
      message.empty();
      message.html(msg);
      message.fadeIn();

      window.setTimeout(function(){
        message.fadeOut('fast', function(){
          if(error) {
            $(this).removeClass('error');
          }
        });
      }, (error ? 6000 : 3000));

    },
    applyFilter: function(type, filter) {
      $('.reviews-block [data-filter-' + type + '*="' + filter + '"]').addClass('filtred').hide();
      $('.review-block').each(function(){
        var hasUnFiltred = $('.review-item:not(.filtred)', this).size();
        if(hasUnFiltred == 0) {
          $(this).addClass('filtred').hide();
        }
      });
    },
    removeFilter: function(type, filter) {
      var current = $('#pdfviewer .page-' + Drupal.PDFReview.page);
      current.find('.reviews-block [data-filter-' + type + '*="' + filter + '"]').removeClass('filtred').show();
    },
    resetFilters: function(callback) {
      $('.reviews-block .filtred.hidden').removeClass('filtred');
      $('.reviews-block .filtred:not(.hidden)').removeClass('filtred').show();          
      if(typeof callback == 'function') {
        callback.call();
      }
    },
    read: function(rid){
      $.get(Drupal.settings.basePath + 'reviews/read', {'doc': Drupal.settings.PDFViewer.doc, 'rid': rid});
    },
    dialogManage: function(url) {
      var self = this;
      $('body').find('#PDFViewerDialog').remove().end().prepend('<div title="'+ Drupal.t("PDF Review Manage") +'" id="PDFViewerDialog"></div>');
      $('#PDFViewerDialog').dialog({
        bgiframe: false,
        height: 400,
        width: 700,
        modal: true,
        draggable: false,
        resizable: false,  
        overlay: {
          backgroundColor: '#000',
          opacity: 0.5
        }
      }).load(url, function(){
        Drupal.PDFReview.initManageChapters();
        
        $('#PDFViewerDialog form').submit(function(e){
          Drupal.PDFReview.prepareChaptersForSave();
          var form = $(this);
          var params = form.serialize();
          if(params.length > 0) {
            $.post(url, params, function(data){
              if (data.message) {
                self.message(data.message, data.error);
                if(!data.error) {
                  $('#PDFViewerDialog').dialog('close');
                }
              }
            }, 'json');
          }
          e.preventDefault();
          return false;
        });
      });
      return false;
    },
    
    /**
     * Manage Chapters
     */
    initManageChapters: function() {
      $textarea = $('#PDFViewerDialog textarea.chapters').hide();
      var chapters = $textarea.val(),
      ui  = '<table class="chapters">';
      ui += '<thead><th class="from">Page From</th><th class="to">To</th><th class="title">Chapter Title</th></thead>';
      ui += '<tbody>';
      
      // Parse chapters format FROM:TO:TITLE, e.g. 1:3:Chapter 1
      chapters = chapters.split("\n");
      // Add three extra rows for new chapters
      chapters.push('::');
      chapters.push('::');
      chapters.push('::');

      // Render chapters
      for(i in chapters) {
        if (chapters[i] == "") { 
          continue; 
        }
        chapter = chapters[i].split(":");        
        ui += Drupal.PDFReview.renderChapterRow(chapter);
      }
      
      ui += '  <tr class="add"><td colspan="3"><a href="javascript:Drupal.PDFReview.addChapters();">+ more chapters</a></td></tr>';
      ui += '</tbody>';
      ui += '</table>';
      $textarea.before(ui);
    },
    renderChapterRow: function(chapter) {
      html  = '<tr class="chapter">';
      html += ' <td><input type="textfield" value="'+ chapter[0] +'" class="from" /></td>';
      html += ' <td><input type="textfield" value="'+ chapter[1] +'" class="to" /></td>';
      html += ' <td><input type="textfield" value="'+ chapter[2] +'" class="title" /></td>';
      html += '</tr>';
      return html;
    },
    addChapters: function() {
      var rows = '';
      // Add three rows for new chapters
      for (var i=1; i <= 3; i++) {
        rows += Drupal.PDFReview.renderChapterRow(['', '', '']);
      };
      $('#PDFViewerDialog tr.add').before(rows);
      return false;
    },
    prepareChaptersForSave: function() {
      var chapters = [];
      $('#PDFViewerDialog table.chapters tbody tr.chapter').each(function(){
        if ($(this).find('.from').val() != '') {
          chapters.push($(this).find('.from').val() +':'+ $(this).find('.to').val() +':'+ $(this).find('.title').val());
        }
      });
      
      $('#PDFViewerDialog textarea.chapters').text(chapters.join("\n"));
    },
    getRegion: function(obj, position) {
      var number = 8;
      height = parseInt(obj.height());
      if (height <= 0) {
        height = 1;
      }

      var region_size = height / number;
      if (position <= 0) {
        position = 1;
      }

      var region = (position / region_size);
      return Math.round(region)
    },
    isFormHidden: function() {
      return (parseInt(jQuery('#review-form').css('left')) + parseInt(jQuery('#review-form').width())) > parseInt(jQuery('#pdfviewer').width());
    },
    setNewLeft: function(left) {
      $('#review-form div.triangle').css('margin-left', '25px');
      var a = parseInt($('#pdfviewer').width()) - parseInt(left);
      var b = parseInt($('#review-form').width()) - a;
      var c = (parseInt(left) - b) - 95;
      // var d = parseInt($('#review-form').width()) - 40;
      $('#review-form').css('left', c + 'px');
      $('#review-form div.triangle').css('margin-left', (b + 25 + 95) + 'px');
    }
  };
})(jQuery);
